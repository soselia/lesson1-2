//დაწერეთ ფუნქცია, რომელსაც გადაეცემა String ტიპის პარამეტრი და დააბრუნებს Boolean
//მნიშვნელობას.ფუნქციამ უნდა დააბრუნოს true, თუ გადმოცემული String მნიშვნელობა
//პალინდრომია, წინააღმდეგ შემთხვევაში false.
//(პალინდრომი— სიტყვა, ფრაზა ან ლექსი, რომელიც წაღმა და უკუღმა ერთნაირად იკითხება)

fun main():Unit{
    val stringInput = readLine()
    if (stringInput != null) {
        ex2(stringInput)
    }
}

fun ex2(string: String):Boolean{
    var reverse = string.reversed()

    if (string == reverse) {
        println("True")
        return true
    }
    else {
        println("False")
        return false
    }
}